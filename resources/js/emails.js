const emailContentElem = document.getElementById('email-modal-content');
let ckEditor;

/*
 * Functions
 */
const getEmailContent = async emailId => {
    const response = await fetch(`/emails/${emailId}`, {
        headers: {
            'Accept': 'application/json'
        }
    })

    return await response.json();
}

const displayEmailContent = content => {
    ckEditor.setData(content);
    $('#email-modal').modal('show');
};

/*
 * CKEDITOR
 */
ClassicEditor
    .create(emailContentElem)
    .then(newEditor => {
        ckEditor = newEditor;
    })
    .catch(error => {
        console.error(error);
    });

/*
 * Emails Datatable
 */
$(document).ready(() => {
    let oldStart = 0;
    const table = $('#emails').DataTable({
        processing: true,
        serverSide: true,
        bFilter: false,
        lengthChange: false,
        order: [[2, 'desc']],
        ajax: {
            url: '/emails/load',
            data: data => {
                data.label = $('#label').val()
            }
        },
        fnDrawCallback: (oSettings) => {
            if (oSettings._iDisplayStart !== oldStart) {
                $('html, body').animate({
                    scrollTop: $('.dataTables_wrapper').offset().top
                }, 'slow');
                oldStart = oSettings._iDisplayStart;
            }
        },
        columns: [
            {data: 'from'},
            {data: 'subject'},
            {data: 'date'},
            {data: 'labels', name: 'labels.name'},
            {data: 'action'},
        ],
        columnDefs: [
            {orderable: false, targets: [3, 4]}
        ]
    });

    table.on('click', '.js-view', (e) => {
        const emailId = e.target.value;

        getEmailContent(emailId)
            .then(data => {
                displayEmailContent(data.content);
            });
    });

    $('#label').change(() => {
        table.draw();
    });
});
