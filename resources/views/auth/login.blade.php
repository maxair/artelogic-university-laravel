@extends('layouts.app')
@section('content')
    <div class="container vh-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4 mt-1 text-center">Log In</h4>
                    <p>You’re just a few clicks away from a Gmailer.</p>
                    <p>
                        <a href="{{ route('oauth-gmail') }}" class="btn btn-block btn-outline-danger"><i
                                class="fab fa-google"></i>   Login with Google</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection




