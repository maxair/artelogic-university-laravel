<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class NoAccessTokenException extends Exception
{
    /**
     * Redirect user to login page
     *
     * @return Redirector|RedirectResponse
     */
    public function render()
    {
        return redirect('login');
    }
}
