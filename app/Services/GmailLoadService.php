<?php


namespace App\Services;

use App\Gmail;
use App\Label;
use Dacastro4\LaravelGmail\Facade\LaravelGmail;
use Dacastro4\LaravelGmail\Services\MessageCollection;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class GmailLoadService
{
    /**
     * Paging first record indicator.
     *
     * @var int
     */
    private int $start;

    /**
     * The number of records that the table can display in the current draw.
     *
     * @var int
     */
    private int $length;

    /**
     * The selected email label
     *
     * @var string|null
     */
    private ?string $label;

    /**
     * The authenticated user.
     *
     * @var Authenticatable|null
     */
    private ?Authenticatable $user;

    /**
     * GmailLoadService constructor.
     * @param  array  $data
     */
    public function __construct(array $data)
    {
        $this->start = (int) $data['start'];
        $this->length = (int) $data['length'];
        $this->label = $data['label'];
        $this->user = Auth::user();
    }

    /**
     * Loads user emails.
     *
     * @return JsonResponse
     */
    public function getEmails(): JsonResponse
    {
        if ($this->isEmptyInbox()) {
            $this->loadAndSaveEmails();
        } elseif ($this->isMoreEmailsNeeded()) {
            $nextPageToken = $this->user->next_page_token;

            if (!is_null($nextPageToken)) {
                $this->loadAndSaveEmails($nextPageToken);
            }
        }

        return $this->loadEmailsFromDatabase();
    }

    /**
     * Checks if user's inbox is empty.
     *
     * @return bool TRUE if empty, FALSE otherwise
     */
    private function isEmptyInbox(): bool
    {
        return $this->countEmails() === 0;
    }

    /**
     * Counts total number of inbox emails.
     *
     * @return int
     */
    private function countEmails(): int
    {
        return Gmail::where('user_id', $this->user->id)->count();
    }

    /**
     * Loads emails from Gmail API and saves them into a database.
     *
     * @param  string|null  $nextPageToken
     */
    private function loadAndSaveEmails(?string $nextPageToken = null): void
    {
        $emails = $this->loadEmailsFromApi($nextPageToken);
        $this->updateNextPageToken($emails->getPageToken());
        $this->saveEmails($emails);
    }

    /**
     * Loads emails from Gmail API.
     *
     * @param  string|null  $nextPageToken
     * @return MessageCollection
     */
    private function loadEmailsFromApi(?string $nextPageToken = null): MessageCollection
    {
        return LaravelGmail::message()->take(20)->preload()->in('inbox')->all($nextPageToken);
    }

    /**
     * Loads emails from database.
     *
     * @return JsonResponse
     */
    private function loadEmailsFromDatabase(): JsonResponse
    {
        $emails = Gmail::with('labels')->where('user_id', $this->user->id);

        return DataTables::eloquent($emails)
            ->addColumn('labels', function (Gmail $email) {
                return $this->renderLabels($email->labels);
            })
            ->addColumn('action', function (Gmail $email) {
                return $this->renderAction($email->id);
            })
            ->filter(function ($query) {
                $this->filterByLabel($query);
            })
            ->rawColumns(['labels', 'action'])
            ->toJson();
    }

    /**
     * Renders labels.
     *
     * @param  Collection  $labels
     * @return string
     */
    private function renderLabels(Collection $labels): string
    {
        return $labels->map(function ($label) {
            return '<span class="badge badge-secondary">'.$label->name.'</span>';
        })->implode('</br>');
    }

    /**
     * Render action button.
     *
     * @param  string  $emailId
     * @return string
     */
    private function renderAction(string $emailId): string
    {
        return '<button type="button" class="js-view btn btn-primary text-center" value="'.$emailId.'">View</button>';
    }

    /**
     * Filters emails by selected label.
     *
     * @param $query
     */
    private function filterByLabel($query): void
    {
        $selectedLabel = $this->label;

        if (in_array($selectedLabel, Config::get('constants.gmails.filter_labels'))) {
            $query->whereHas('labels', function ($q) use ($selectedLabel) {
                $q->where('name', $selectedLabel);
            });
        }
    }

    /**
     * Updates nextPageToken.
     *
     * @param  string|null  $nextPageToken
     */
    private function updateNextPageToken(?string $nextPageToken): void
    {
        $this->user->next_page_token = $nextPageToken;
        $this->user->save();
    }

    /**
     * Saves emails into database.
     *
     * @param  MessageCollection  $emails
     */
    private function saveEmails(MessageCollection $emails): void
    {
        $gmails = [];
        $gmailLabel = [];

        foreach ($emails as $email) {
            $emailData['id'] = $email->getId();
            $emailData['user_id'] = $this->user->id;
            $emailData['from'] = $email->getFromName();
            $emailData['subject'] = $email->getSubject();
            $emailData['content'] = $email->getHtmlBody();
            $emailData['date'] = $email->getDate();

            $gmails[] = $emailData;
            $labelIds = $this->createLabels($email->getLabels());

            foreach ($labelIds as $labelId) {
                $gmailLabel[] = ['gmail_id' => $email->getId(), 'label_id' => $labelId];
            }
        }

        DB::table('gmails')->insert($gmails);
        DB::table('gmail_label')->insert($gmailLabel);
    }

    /**
     * Creates labels.
     *
     * @param  array  $labelNames
     * @return array
     */
    private function createLabels(array $labelNames): array
    {
        $labelIds = [];

        foreach ($labelNames as $labelName) {
            $labelIds[] = Label::firstOrCreate(['name' => $labelName])->id;
        }

        return $labelIds;
    }

    /**
     * Checks if it necessary to load more emails from Gmail API.
     *
     * @return bool
     */
    private function isMoreEmailsNeeded(): bool
    {
        return $this->countEmails() <= ($this->start + $this->length);
    }
}
