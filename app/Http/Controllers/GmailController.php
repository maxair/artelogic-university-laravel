<?php

namespace App\Http\Controllers;

use App\Gmail;
use App\Http\Requests\GetEmailsRequest;
use App\Services\GmailLoadService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Illuminate\View\View;

class GmailController extends Controller
{
    /**
     * Display user emails.
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('emails.index', [
            'labels' => Config::get('constants.gmails.filter_labels'),
        ]);
    }

    /**
     * Load content for a selected email.
     *
     * @param  Gmail  $email
     * @return JsonResponse
     */
    public function getEmailContent(Gmail $email): JsonResponse
    {
        return response()->json(['content' => $email->content]);
    }

    /**
     * Load emails.
     *
     * @param  GetEmailsRequest  $request
     * @return JsonResponse
     */
    public function getEmails(GetEmailsRequest $request): JsonResponse
    {
        $gmailLoadService = new GmailLoadService($request->validated());

        return $gmailLoadService->getEmails();
    }

}
